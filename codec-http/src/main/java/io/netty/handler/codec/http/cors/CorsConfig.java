/*
 * Copyright 2013 The Netty Project
 *
 * The Netty Project licenses this file to you under the Apache License, version
 * 2.0 (the "License"); you may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package io.netty.handler.codec.http.cors;

import io.netty.handler.codec.http.DefaultHttpHeaders;
import io.netty.handler.codec.http.EmptyHttpHeaders;
import io.netty.handler.codec.http.HttpHeaders;
import io.netty.handler.codec.http.HttpMethod;
import io.netty.util.internal.StringUtil;

import java.util.Collections;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.Callable;

/**
 * Configuration for Cross-Origin Resource Sharing (CORS).
 * 对跨域资源共享进行配置。
 */
public final class CorsConfig {

    private final Set<String> origins;
    private final boolean anyOrigin;
    private final boolean enabled;
    private final Set<String> exposeHeaders;
    private final boolean allowCredentials;
    private final long maxAge;
    private final Set<HttpMethod> allowedRequestMethods;
    private final Set<String> allowedRequestHeaders;
    private final boolean allowNullOrigin;
    private final Map<CharSequence, Callable<?>> preflightHeaders;
    private final boolean shortCircuit;

    CorsConfig(final CorsConfigBuilder builder) {
        origins = new LinkedHashSet<String>(builder.origins);
        anyOrigin = builder.anyOrigin;
        enabled = builder.enabled;
        exposeHeaders = builder.exposeHeaders;
        allowCredentials = builder.allowCredentials;
        maxAge = builder.maxAge;
        allowedRequestMethods = builder.requestMethods;
        allowedRequestHeaders = builder.requestHeaders;
        allowNullOrigin = builder.allowNullOrigin;
        preflightHeaders = builder.preflightHeaders;
        shortCircuit = builder.shortCircuit;
    }

    /**
     * Determines if support for CORS is enabled.
     * 决定是否支持跨域资源共享。
     * @return {@code true} if support for CORS is enabled, false otherwise.
     */
    public boolean isCorsSupportEnabled() {
        return enabled;
    }

    /**
     * Determines whether a wildcard origin, '*', is supported.
     * 决定是否支持通配符*。
     * @return {@code boolean} true if any origin is allowed.
     */
    public boolean isAnyOriginSupported() {
        return anyOrigin;
    }

    /**
     * Returns the allowed origin. This can either be a wildcard or an origin value.
     * 返回允许访问的源。可以被设置为通配符或者具体的源。
     * @return the value that will be used for the CORS response header 'Access-Control-Allow-Origin'
     *         返回的值会被Access-Control-Allow-Origin响应头使用。
     */
    public String origin() {
        return origins.isEmpty() ? "*" : origins.iterator().next();
    }

    /**
     * Returns the set of allowed origins.
     * 返回一组允许访问的源。
     * @return {@code Set} the allowed origins.
     */
    public Set<String> origins() {
        return origins;
    }

    /**
     * Web browsers may set the 'Origin' request header to 'null' if a resource is loaded
     * from the local file system.
     * 如果资源从本地文件系统加载的话，Web浏览器可能设置Origin请求头为null。
     * If isNullOriginAllowed is true then the server will response with the wildcard for the
     * the CORS response header 'Access-Control-Allow-Origin'.
     * 如果该方法返回true，服务器将会将响应头Access-Control-Allow-Origin的值设置为通配符。
     * @return {@code true} if a 'null' origin should be supported.
     */
    public boolean isNullOriginAllowed() {
        return allowNullOrigin;
    }

    /**
     * Returns a set of headers to be exposed to calling clients.
     * 返回一组可以暴露给调用客户端的响应头(即客户端JS可以从浏览器获取的响应头)。
     * During a simple CORS request only certain response headers are made available by the
     * browser, for example using:
     * 在简单的CORS请求期间，浏览器仅提供某些响应头。
     * <pre>
     * xhr.getResponseHeader("Content-Type");
     * </pre>
     * The headers that are available by default are:
     * 下面这些消息头是默认有效的：
     * <ul>
     * <li>Cache-Control</li>
     * <li>Content-Language</li>
     * <li>Content-Type</li>
     * <li>Expires</li>
     * <li>Last-Modified</li>
     * <li>Pragma</li>
     * </ul>
     * To expose other headers they need to be specified, which is what this method enables by
     * adding the headers names to the CORS 'Access-Control-Expose-Headers' response header.
     * 暴露其它的请求头需要特殊指定，将这些被允许的消息头添加到Access-Control-Expose-Headers响应头上。
     * @return {@code List<String>} a list of the headers to expose.
     */
    public Set<String> exposedHeaders() {
        return Collections.unmodifiableSet(exposeHeaders);
    }

    /**
     * Determines if cookies are supported for CORS requests.
     * 用来判断跨域资源共享请求是否支持Cookies。
     * By default cookies are not included in CORS requests but if isCredentialsAllowed returns
     * true cookies will be added to CORS requests. Setting this value to true will set the
     * CORS 'Access-Control-Allow-Credentials' response header to true.
     * 默认情况下Cookies不会被包含在跨域资源共享请求里面，但是如果该方法返回true跨域资源共享请求就可以包含Cookies。
     * 设置这个值为true就是设置Access-Control-Allow-Credentials的值为true。
     * Please note that cookie support needs to be enabled on the client side as well.
     * 客户端也需要同时支持才能发送Cookie。
     * The client needs to opt-in to send cookies by calling:
     * 客户端需要像下面这样配置：
     * <pre>
     * xhr.withCredentials = true;
     * </pre>
     * The default value for 'withCredentials' is false in which case no cookies are sent.
     * Setting this to true will included cookies in cross origin requests.
     * withCredentials的默认值是false，不会发送Cookie。当设置为true时才会将Cookies放到请求头中。
     * @return {@code true} if cookies are supported.
     */
    public boolean isCredentialsAllowed() {
        return allowCredentials;
    }

    /**
     * Gets the maxAge setting.
     * 获取maxAge设置。
     * When making a preflight request the client has to perform two request with can be inefficient.
     * This setting will set the CORS 'Access-Control-Max-Age' response header and enables the
     * caching of the preflight response for the specified time. During this time no preflight
     * request will be made.
     * 当需要发送前置请求时，客户端需要每次需要发送两个请求，效率很低。这个设置将会赋值给Access-Control-Max-Age响应头，
     * 让客户端在指定时间缓存前置响应的值。在这个时间内客户端不需要发送前置请求。
     * @return {@code long} the time in seconds that a preflight request may be cached.
     *          前置请求可以缓存的秒数。
     */
    public long maxAge() {
        return maxAge;
    }

    /**
     * Returns the allowed set of Request Methods. The Http methods that should be returned in the
     * CORS 'Access-Control-Request-Method' response header.
     * 返回允许的请求方法，这个函数的返回值将会被设置到Access-Control-Request-Method响应头上。
     * @return {@code Set} of {@link HttpMethod}s that represent the allowed Request Methods.
     */
    public Set<HttpMethod> allowedRequestMethods() {
        return Collections.unmodifiableSet(allowedRequestMethods);
    }

    /**
     * Returns the allowed set of Request Headers.
     * 返回允许的一组请求头。
     * The header names returned from this method will be used to set the CORS
     * 'Access-Control-Allow-Headers' response header.
     * 该方法的返回值将会被设置的Access-Control-Allow-Headers响应头上。
     * @return {@code Set<String>} of strings that represent the allowed Request Headers.
     */
    public Set<String> allowedRequestHeaders() {
        return Collections.unmodifiableSet(allowedRequestHeaders);
    }

    /**
     * Returns HTTP response headers that should be added to a CORS preflight response.
     * 返回应该被添加到跨域资源共享的预请求的响应上的HTTP响应头。
     * @return {@link HttpHeaders} the HTTP response headers to be added.
     */
    public HttpHeaders preflightResponseHeaders() {
        if (preflightHeaders.isEmpty()) {
            return EmptyHttpHeaders.INSTANCE;
        }
        final HttpHeaders preflightHeaders = new DefaultHttpHeaders();
        for (Entry<CharSequence, Callable<?>> entry : this.preflightHeaders.entrySet()) {
            final Object value = getValue(entry.getValue());
            if (value instanceof Iterable) {
                preflightHeaders.add(entry.getKey(), (Iterable<?>) value);
            } else {
                preflightHeaders.add(entry.getKey(), value);
            }
        }
        return preflightHeaders;
    }

    /**
     * Determines whether a CORS request should be rejected if it's invalid before being
     * further processing.
     *
     * CORS headers are set after a request is processed. This may not always be desired
     * and this setting will check that the Origin is valid and if it is not valid no
     * further processing will take place, and an error will be returned to the calling client.
     *
     * @return {@code true} if a CORS request should short-circuit upon receiving an invalid Origin header.
     */
    public boolean isShortCircuit() {
        return shortCircuit;
    }

    /**
     * @deprecated Use {@link #isShortCircuit()} instead.
     */
    @Deprecated
    public boolean isShortCurcuit() {
        return isShortCircuit();
    }

    private static <T> T getValue(final Callable<T> callable) {
        try {
            return callable.call();
        } catch (final Exception e) {
            throw new IllegalStateException("Could not generate value for callable [" + callable + ']', e);
        }
    }

    @Override
    public String toString() {
        return StringUtil.simpleClassName(this) + "[enabled=" + enabled +
                ", origins=" + origins +
                ", anyOrigin=" + anyOrigin +
                ", exposedHeaders=" + exposeHeaders +
                ", isCredentialsAllowed=" + allowCredentials +
                ", maxAge=" + maxAge +
                ", allowedRequestMethods=" + allowedRequestMethods +
                ", allowedRequestHeaders=" + allowedRequestHeaders +
                ", preflightHeaders=" + preflightHeaders + ']';
    }

    /**
     * @deprecated Use {@link CorsConfigBuilder#forAnyOrigin()} instead.
     */
    @Deprecated
    public static Builder withAnyOrigin() {
        return new Builder();
    }

    /**
     * @deprecated Use {@link CorsConfigBuilder#forOrigin(String)} instead.
     */
    @Deprecated
    public static Builder withOrigin(final String origin) {
        if ("*".equals(origin)) {
            return new Builder();
        }
        return new Builder(origin);
    }

    /**
     * @deprecated Use {@link CorsConfigBuilder#forOrigins(String...)} instead.
     */
    @Deprecated
    public static Builder withOrigins(final String... origins) {
        return new Builder(origins);
    }

    /**
     * @deprecated Use {@link CorsConfigBuilder} instead.
     */
    @Deprecated
    public static class Builder {

        private final CorsConfigBuilder builder;

        /**
         * @deprecated Use {@link CorsConfigBuilder} instead.
         */
        @Deprecated
        public Builder(final String... origins) {
            builder = new CorsConfigBuilder(origins);
        }

        /**
         * @deprecated Use {@link CorsConfigBuilder} instead.
         */
        @Deprecated
        public Builder() {
            builder = new CorsConfigBuilder();
        }

        /**
         * @deprecated Use {@link CorsConfigBuilder#allowNullOrigin()} instead.
         */
        @Deprecated
        public Builder allowNullOrigin() {
            builder.allowNullOrigin();
            return this;
        }

        /**
         * @deprecated Use {@link CorsConfigBuilder#disable()} instead.
         */
        @Deprecated
        public Builder disable() {
            builder.disable();
            return this;
        }

        /**
         * @deprecated Use {@link CorsConfigBuilder#exposeHeaders(String...)} instead.
         */
        @Deprecated
        public Builder exposeHeaders(final String... headers) {
            builder.exposeHeaders(headers);
            return this;
        }

        /**
         * @deprecated Use {@link CorsConfigBuilder#allowCredentials()} instead.
         */
        @Deprecated
        public Builder allowCredentials() {
            builder.allowCredentials();
            return this;
        }

        /**
         * @deprecated Use {@link CorsConfigBuilder#maxAge(long)} instead.
         */
        @Deprecated
        public Builder maxAge(final long max) {
            builder.maxAge(max);
            return this;
        }

        /**
         * @deprecated Use {@link CorsConfigBuilder#allowedRequestMethods(HttpMethod...)} instead.
         */
        @Deprecated
        public Builder allowedRequestMethods(final HttpMethod... methods) {
            builder.allowedRequestMethods(methods);
            return this;
        }

        /**
         * @deprecated Use {@link CorsConfigBuilder#allowedRequestHeaders(String...)} instead.
         */
        @Deprecated
        public Builder allowedRequestHeaders(final String... headers) {
            builder.allowedRequestHeaders(headers);
            return this;
        }

        /**
         * @deprecated Use {@link CorsConfigBuilder#preflightResponseHeader(CharSequence, Object...)} instead.
         */
        @Deprecated
        public Builder preflightResponseHeader(final CharSequence name, final Object... values) {
            builder.preflightResponseHeader(name, values);
            return this;
        }

        /**
         * @deprecated Use {@link CorsConfigBuilder#preflightResponseHeader(CharSequence, Iterable)} instead.
         */
        @Deprecated
        public <T> Builder preflightResponseHeader(final CharSequence name, final Iterable<T> value) {
            builder.preflightResponseHeader(name, value);
            return this;
        }

        /**
         * @deprecated Use {@link CorsConfigBuilder#preflightResponseHeader(CharSequence, Callable)} instead.
         */
        @Deprecated
        public <T> Builder preflightResponseHeader(final String name, final Callable<T> valueGenerator) {
            builder.preflightResponseHeader(name, valueGenerator);
            return this;
        }

        /**
         * @deprecated Use {@link CorsConfigBuilder#noPreflightResponseHeaders()} instead.
         */
        @Deprecated
        public Builder noPreflightResponseHeaders() {
            builder.noPreflightResponseHeaders();
            return this;
        }

        /**
         * @deprecated Use {@link CorsConfigBuilder#build()} instead.
         */
        @Deprecated
        public CorsConfig build() {
            return builder.build();
        }

        /**
         * @deprecated Use {@link CorsConfigBuilder#shortCircuit()} instead.
         */
        @Deprecated
        public Builder shortCurcuit() {
            builder.shortCircuit();
            return this;
        }
    }

    /**
     * @deprecated Removed without alternatives.
     */
    @Deprecated
    public static final class DateValueGenerator implements Callable<Date> {

        @Override
        public Date call() throws Exception {
            return new Date();
        }
    }
}
